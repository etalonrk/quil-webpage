# Change Log
All notable changes to this project will be documented in this file. This change log follows the conventions of [keepachangelog.com](http://keepachangelog.com/).

## Unreleased

## 0.1.3 - 2017-07-02
### Fixed
- didn't pass a function to DOMContentLoaded event

## 0.1.2 - 2017-06-25
### Modified
- :host entry is removed from the defsketch

### Fixed
- typos

## 0.1.1 - 2017-06-23
### Modified
- the .gitignore for the template wasn't the good one

## 0.1.0 - 2017-06-23
### Added
- Basic template working with figwheel out of the box
- Files from the new template.
