(ns {{name}}.core
  (:require [{{name}}.sketch :as sketch]))

(.addEventListener js/document "DOMContentLoaded" #(sketch/{{name}}))
